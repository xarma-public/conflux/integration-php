# Conflux Integration

A collection of interfaces used to define integrations for [Xarma Conflux](https://xarma.co/conflux).

## Usage

```bash
$ composer require xarma/conflux-integration
```
