<?php

namespace Xarma\Conflux\Integration;

use Psr\Log\LoggerInterface;

interface Integration
{
    /**
     * @see \Psr\Log\LoggerAwareTrait
     */
    public function setLogger(LoggerInterface $logger): void;
}