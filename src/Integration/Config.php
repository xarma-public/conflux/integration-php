<?php

namespace Xarma\Conflux\Integration\Integration;

use Xarma\Conflux\Integration\Config\Auth;

interface Config
{
    /**
     * Gets an Integration's configuration by key
     *
     * @param string $key
     *
     * @return mixed
     */
    public function get(string $key): mixed;

    /**
     * Gets authentication configuration for an Integration
     *
     * @return Auth
     */
    public function getAuth(): Auth;
}